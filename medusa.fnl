#!/usr/bin/env fennel
;;; commentary

;;; code

(local repoPath (.. (os.getenv "XDG_CONFIG_DIR") "medusa/repos"))


(local medusa
       {
        :arch {}
        :debian {}
        :pacstall {}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        :cargo {}
        :lua {}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        :git {}
})
;;;; main functions
;;;;; arch
(fn medusa.arch.install [packagename]
  (print (.. "installing " packagename "..."))
  (os.execute "sleep 1")
  (if (os.execute (.."paru -S --needed " packagename))
      (print (.. "installed " packagename))
      (print (.. "installation of " packagename " failed"))
      )
  )
(fn medusa.arch.remove [packagename]
  (print (.. "uninstalling " packagename "..."))
  (os.execute "sleep 1")
  (if (os.execute (.."paru -R " packagename))
      (print (.. "uninstalled " packagename))
      (print (.. "uninstallation of " packagename " failed"))
      )
  )
;;;;; debian (and *buntu)
(fn medusa.debian.install [packagename]
  (print (.. "installing " packagename "..."))
  (os.execute "sleep 1")
  (local installed
         (os.execute
          (..
           "aptitude --simulate --assume-yes install "
           packagename
           "|grep 'already installed'" )
          )
         )
  (when (= installed 1)
  (if (os.execute (.."sudo aptitude install " packagename))
      (print (.. "installed " packagename))
      (print (.. "installation of " packagename " failed"))
      )
    )
  )
(fn medusa.debian.remove [packagename]
  (print (.. "uninstalling " packagename "..."))
  (os.execute "sleep 1")
  (if (os.execute (.."sudo aptitude remove " packagename))
      (print (.. "uninstalled " packagename))
      (print (.. "uninstallation of " packagename " failed"))
      )
  )
;;;;; pacstall
(fn medusa.pacstall.install [packagename]
  (print (.. "installing " packagename "..."))
  (os.execute "sleep 1")
  (if (os.execute (.."pacstall -I " packagename))
      (print (.. "installed " packagename))
      (print (.. "installation of " packagename " failed"))
      )
  )
(fn medusa.pacstall.remove [packagename]
  (print (.. "uninstalling " packagename "..."))
  (os.execute "sleep 1")
  (if (os.execute (.."pacstall -R " packagename))
      (print (.. "uninstalled " packagename))
      (print (.. "uninstallation of " packagename " failed"))
      )
  )


;;;;; cargo
(fn medusa.cargo.install [packagename]
  (print (.. "installing " packagename "..."))
  (os.execute "sleep 1")
  (if (os.execute (.."cargo install " packagename))
      (print (.. "installed " packagename))
      (print (.. "installation of " packagename " failed"))
      1
      )
  )
(fn medusa.cargo.remove [packagename]
  (print (.. "uninstalling " packagename "..."))
  (os.execute "sleep 1")
  (if (os.execute (.."cargo uninstall " packagename))
      (print (.. "uninstalled " packagename))
      (print (.. "uninstallation of " packagename " failed"))
      1
      )
  )
;;;;; lua
(fn medusa.lua.install [packagename]
  (print (.. "installing " packagename "..."))
  (os.execute "sleep 1")
  (if (os.execute (.."luarocks install --local " packagename))
      (print (.. "installed " packagename))
      (print (.. "installation of " packagename " failed"))
      1
      )
  )
(fn medusa.lua.remove [packagename]
  (print (.. "uninstalling " packagename "..."))
  (os.execute "sleep 1")
  (if (os.execute (.."luarocks remove --local " packagename))
      (print (.. "uninstalled " packagename))
      (print (.. "uninstallation of " packagename " failed"))
      1
      )
  )

(fn medusa.git.install [repo packagename]
  (local packageroot (.. repoPath "/" packagename))
  (print (.. "cloning " repo " into " packageroot))
  (os.execute (table.concat ["git" "clone" repo packageroot] " "))
  (os.execute (.. "make -C " packageroot " "))
  (os.execute (table.concat ["sudo" "make" "-C" packageroot "clean" "install"] " "))
  )





medusa
