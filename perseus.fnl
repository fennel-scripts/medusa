#!/usr/bin/env fennel

(local medusa (require :medusa))

;;;; aliases

;;(local handle (io.popen "awk -F '=' '/^ID_LIKE=/ {print $2}' /etc/*-release|tr -d '\n'"))
;;(local dist (handle:read "*a"))
;;(handle:close)
;;(print dist)

(local dist (. arg 1))


(when (= dist "arch")
  (fn medusa.get [pkg] (medusa.arch.install pkg))
  (fn medusa.rm [pkg]  (medusa.arch.remove pkg))
  )
(when (= dist "debian")
  (fn medusa.get [pkg] (medusa.debian.install pkg))
  (fn medusa.rm [pkg]  (medusa.debian.remove pkg))
  (fn medusa.pacstall.get [pkg] (medusa.pacstall.install pkg))
  (fn medusa.pacstall.rm [pkg] (medusa.pacstall.remove pkg))
  )
  (fn medusa.cargo.get [pkg] (medusa.cargo.install pkg))
  (fn medusa.cargo.rm [pkg]  (medusa.cargo.remove pkg))

  (fn medusa.lua.get [pkg] (medusa.lua.install pkg))
  (fn medusa.lua.rm [pkg]  (medusa.lua.remove pkg))

;;; example: install a package

(fn a []

(medusa.get "lua5.4")
(medusa.get "awesome")
(medusa.get "emacs")
(medusa.get "pcmanfm-qt")
(medusa.lua.get "fennel")
(when (= dist "arch")
  (print "arch only")
  (medusa.cargo.get "xplr")
  (medusa.get "wezterm")
  (medusa.get "tym")
  (medusa.get "ly")
  (os.execute "sudo systemctl enable ly.service")
  (print "you can find the config file for ly in /etc/ly/config.ini")
  )
(medusa.get "kitty")
(medusa.get "luakit")
(if (medusa.get "yadm")
    (when (= (. arg 2) "initial")
    (os.execute "yadm clone git@gitlab.com:Erik.Lundstedt/dotconfigfiles.git")
    (os.execute "yadm pull")
    )
)

(when (= dist "debian") (print "debian only"))
)

(fn b []


(medusa.git.install "https://git.sr.ht/~ghost08/photon" "photon")

  )


(b)


;;(medusa.get "")
;;(medusa.get "")
